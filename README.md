# ansible_tower_utils

## Introduction
Ce repository contient une collection Ansible destinée à simplifier la création d'objets TOWER as code.

Les objets TOWER pouvant etre créés sont:
- Credentials
- Inventories
- Projects
- Job Templates

## Utilisation

### Prerequis
- Avoir un role Vault et une policy pour que le repository qui va etre créé puisse récupérer dans Vault les secrets pour interagir avec l'API Ansible TOWER.
- Dans le cas du besoin de création de Credentials dans Tower (Par exemple pour TOWER puisse se connecter aux serveurs, cloner des repo gitlab, télécharger un artefact sur NEXUS...), avoir les secrets correspondants présents dans Vault, et avoir un role Vault et une policy permettant de lire ces secrets.

### Préparation
- Créer un repo gitlab "**CODEAPPLI**_tower_as_code"
  - Exemple: l6_tower_as_code

- Dans ce repo:

  - Créer un .gitlab-ci.yml en prenant ce template en exemple : [.gitlab-ci.yml](/examples/.gitlab-ci.yml)
    - Pensez bien à adapter les informations Vault
<br/><br/>

  - Créer un fichier "special_collections/requirements.yml" avec le contenu suivant: [requirements.yml](/examples/special_collections/requirements.yml)
<br/><br/>

  - Créer les fichiers YAML de vos inventaires dans un dossier "inventories" (1 fichier par environnement).
    - Exemple: [PRD.yml](/examples/inventories/PRD.yml)
<br/><br/>

  - Créer un fichier "group_vars/all.yml" contenant la définition des objects TOWER à créer.
    - Exemple: [all.yml](/examples/group_vars/all.yml)
      - Le fichier exemple contient des explications
    - La liste totale des options utilisables est dans le code Ansible:
      - [Credentials](/tower_as_code/playbooks/create_credentials.yml)
      - [Inventories](/tower_as_code/playbooks/create_inventory.yml)
      - [Projects](/tower_as_code/playbooks/create_project.yml)
      - [Job templates](/tower_as_code/playbooks/create_job_template.yml)
<br/><br/>


Votre repository aura la structure suivante:
```
xx_tower_as_code
├── .gitlab-ci.yml
├── README.md
├── group_vars
│   └── all.yml
├── inventories
│   ├── PRD.yml
│   ├── PPR.yml
│   └── DEV.yml
└── special_collections
    └── requirements.yml
```

### Utilisation
Chaque fois qu'il y aura un push sur ce repository, un pipeline sera déclenché qui s'assurera que les objets TOWER définis dans all.yml correspondent bien à ceux présents sur TOWER.
Les objets absents seront créés, et les objets différents seront mis à jour.
